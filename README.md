Curso Creando Apps. Aprende a programar aplicaciones móviles.
Practica Sensores: Juego

Ejecutar: Phonegap serve (desde el directorio práctica)

Si se quiere instalar la app en el movil, la apk compilada está en el directorio:
\practica\platforms\android\app\build\outputs\apk\debug

El juego consiste en recoger las bolas que van cayendo antes de que toquen el suelo. Tienes 5 vidas
que se irán restando cada vez que una bola toca el suelo. La forma de mover la cesta es inclinando
el movil hacía la izquierda y derecha. Al final de la partida se muestra los records conseguidos.