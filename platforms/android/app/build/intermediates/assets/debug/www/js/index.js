var app = {
	inicio: function (){
		
		window.screen.orientation.lock('portrait');
		alto = document.documentElement.clientHeight;
		ancho = document.documentElement.clientWidth;
		D_barra = 25;
		velocidadX = 0;
		puntuacion = 0;
		vidas = 5;
		gravedadAste = 1;
		app.vigilaSensores();
		app.iniciaJuego();
		min = 20;
		max = 40;
		limite = 5;
		frec = 0;
		//maxP = localStorage.getItem('maxP') || '0';
		user1 = localStorage.getItem('user1') || '----';
		user2 = localStorage.getItem('user2') || '----';
		user3 = localStorage.getItem('user3') || '----';
		punt1 = localStorage.getItem('punt1') || '0';
		punt2 = localStorage.getItem('punt2') || '0';
		punt3 = localStorage.getItem('punt3') || '0';
	},
	
	vigilaSensores: function(){
		
		function onError(){
			console.log('onError');
		}
		function onSuccess(datosAceleracion){
			app.registraPosicion(datosAceleracion);
		}
		navigator.accelerometer.watchAcceleration(onSuccess, onError,{frequency:100});
	},
	
	registraPosicion: function(datosAceleracion){
		velocidadX = datosAceleracion.x;

	},
	
	iniciaJuego: function(){
		
		function preload(){

			//game.stage.backgroundColor = '#f27d0c';
			//Cargamos imagenes
			game.load.image('barra', 'assets/barra.png');
			game.load.image('aste', 'assets/bola.png');
			game.load.image('suelo', 'assets/suelo.png');
			game.load.image('volver', 'assets/volver.png');
			game.load.image('ok', 'assets/ok.png');
			game.load.image('fondo', 'assets/fondo.png');
			game.add.plugin(PhaserInput.Plugin);

		}
		
		function create(){
			
			fondo = game.add.sprite(0, 0, 'fondo');
			fondo.width = ancho;
			fondo.height = alto;
			scoreText = game.add.text(16, 16, "Puntuación: " + puntuacion, { font: '20px Arial', fill: '#805A37' });
			vidaText = game.add.text(200, 16, "Vidas: " + vidas, { font: '20px Arial', fill: '#805A37' });
			//maxpText = game.add.text(16, 60, "Record: " + maxP, { font: '20px Arial', fill: '#805A37' });

			barra = game.add.sprite(app.inicioX(), alto - 50, 'barra');
			suelo = game.add.sprite(0, alto - 30, 'suelo');
			suelo.width = ancho;
			suelo.height = 30;
			
			//Definimos leyes físicas ARCADE
			game.physics.startSystem(Phaser.Physics.ARCADE);
			game.physics.arcade.enable(barra);
			game.physics.arcade.enable(suelo);
			barra.body.collideWorldBounds = true;
			
			asteriscos = game.add.group();
			
		}
		
		function update(){
	
			barra.body.velocity.x = velocidadX*(-300);
			game.physics.arcade.overlap(barra, asteriscos, app.incrementaPuntuacion, null, this);
			game.physics.arcade.overlap(asteriscos, suelo, app.decrementaVida, null, this);
			if (frec == 0){
				if (asteriscos.length < limite){
					frec = setTimeout(app.soltarAste, 1000);
				}
			}
			
		}
		estados = { preload: preload, create: create, update: update };
		game = new Phaser.Game(ancho, alto, Phaser.CANVAS, 'phaser', estados);

	},
	
	numeroAleatorio: function(min, max){
		return Math.round(Math.random() * (max - min) + min);
	},
	
	soltarAste: function(){
		gravedadAste = app.numeroAleatorio(min, max);
		var asterisco = asteriscos.create(app.inicioX(), 25, 'aste');
		game.physics.arcade.enable(asterisco);

		asterisco.body.gravity.y = gravedadAste;
		frec = 0;
	},
	
	incrementaPuntuacion: function(barra, aste){
		aste.destroy();
		puntuacion +=1;
		scoreText.text = "Puntuación: " + puntuacion;
	},
	
	decrementaVida: function(suelo, aste){
		aste.destroy();
		vidas -=1;
		vidaText.text = "Vidas: " + vidas;
		if (vidas == 0){
			suelo.kill();
			barra.kill();
			game.add.text(16, 150, "Game Over!", { font: '52px Arial', fill: '#805A37' });
			
			if(puntuacion > parseInt(punt3)){
				game.add.text(16,240, "Nombre: ",{ font: '16px Arial', fill: '#805A37' });
				app.nombre();
			}else{
				game.add.text(16,240, "Resultados ",{ font: '16px Arial', fill: '#805A37' });
			}
			game.add.button(180, 220, "ok", app.guardar, this);
				
			

			game.add.button(ancho/2 - 35, 550, "volver", app.recargar, this);
			asteriscos.destroy();
		}
	},
	
	nombre: function(){
		
			input = game.add.inputField(80,240,{
			font: '16px Arial',
			fill: '#805A37',
			width: 100,
			pading: 5,
			borderWidth: 1,
			boderColor: '#000',
			borderRadius: 6
		});
		
	},
	
	guardar: function(){
		
		if (puntuacion > parseInt(punt1)){
					game.add.text(16, 100, "Nuevo Record!!:  " + puntuacion, { font: '36px Arial', fill: '#805A37' } );
					user3 = user2;
					punt3 = punt2;
					user2 = user1;
					punt2 = punt1;
					user1 = input.value;
					punt1 = puntuacion;

					
				}else if (puntuacion > parseInt(punt2)){
					user3 = user2;
					punt3 = punt2;
					user2 = input.value;
					punt2 = puntuacion;

				}else if(puntuacion > parseInt(punt3)){
					user3 = input.value;
					punt3= puntuacion;
				}
				
				game.add.text(16,280, "1- ",{ font: '16px Arial', fill: '#805A37' });
				game.add.text(60,280, user1,{ font: '16px Arial', fill: '#805A37' });
				game.add.text(130,280, punt1,{ font: '16px Arial', fill: '#805A37' });
				game.add.text(16,320, "2- ",{ font: '16px Arial', fill: '#805A37' });
				game.add.text(60,320, user2,{ font: '16px Arial', fill: '#805A37' });
				game.add.text(130,320, punt2,{ font: '16px Arial', fill: '#805A37' });
				game.add.text(16,360, "3- ",{ font: '16px Arial', fill: '#805A37' });
				game.add.text(60,360, user3,{ font: '16px Arial', fill: '#805A37' });
				game.add.text(130,360, punt3,{ font: '16px Arial', fill: '#805A37' });
		
		localStorage.setItem('user1', user1);
		localStorage.setItem('punt1', punt1);
		localStorage.setItem('user2', user2);
		localStorage.setItem('punt2', punt2);
		localStorage.setItem('user3', user3);
		localStorage.setItem('punt3', punt3);
		
		//console.log('funciona: ' + input.value );
		
	},
	
	recargar: function(){
		console.log('Recargando...');
		location.reload(true);
	},
	
	inicioX: function(){
		return app.numeroAleatorioHasta(ancho - D_barra );
	},
  
	numeroAleatorioHasta: function(limite){
		return Math.floor(Math.random() * limite);
	}
};

if ('addEventListener' in document) {
    document.addEventListener('deviceready', function() {
        app.inicio();
    }, false);
}
