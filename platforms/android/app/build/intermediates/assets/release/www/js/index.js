var app = {
	inicio: function (){
		
		alto = document.documentElement.clientHeight;
		ancho = document.documentElement.clientWidth;
		D_barra = 25;
		velocidadX = 0;
		puntuacion = 0;
		vidas = 5;
		gravedadAste = 150;
		app.vigilaSensores();
		app.iniciaJuego();
		min = 10;
		max = 100;
		limite = 5;
		frec = 0;
	},
	
	vigilaSensores: function(){
		
		function onError(){
			console.log('onError');
		}
		function onSuccess(datosAceleracion){
			app.registraPosicion(datosAceleracion);
		}
		navigator.accelerometer.watchAcceleration(onSuccess, onError,{frequency:100});
	},
	
	registraPosicion: function(datosAceleracion){
		velocidadX = datosAceleracion.x;

	},
	
	iniciaJuego: function(){
		
		function preload(){
			
			console.log('preload ok');

			game.stage.backgroundColor = '#f27d0c';
			//Cargamos imagenes
			game.load.image('barra', 'assets/barra.png');
			game.load.image('aste', 'assets/objetivo.png');
			game.load.image('suelo', 'assets/suelo.png');

		}
		
		function create(){
			console.log('create ok');
			scoreText = game.add.text(16, 16, "Puntuación: " + puntuacion, { fontsize: '15px', fill: '#757676' });
			vidaText = game.add.text(200, 16, "Vidas: " + vidas, { fontsize: '15px', fill: '#757676' });
			
			barra = game.add.sprite(app.inicioX(), alto - 50, 'barra');
			//aste = game.add.sprite(app.inicioX(), 50, 'aste');
			suelo = game.add.sprite(0, alto - 10, 'suelo');
			suelo.width = ancho;
			suelo.height = 10;
			
			//Definimos leyes físicas ARCADE
			game.physics.startSystem(Phaser.Physics.ARCADE);
			game.physics.arcade.enable(barra);
			//game.physics.arcade.enable(aste);
			game.physics.arcade.enable(suelo);
			barra.body.collideWorldBounds = true;
			
			asteriscos = game.add.group();
			
		}
		
		function update(){
	
			barra.body.velocity.x = velocidadX*(-350);
			game.physics.arcade.overlap(barra, asteriscos, app.incrementaPuntuacion, null, this);
			game.physics.arcade.overlap(asteriscos, suelo, app.decrementaVida, null, this);
			if (frec == 0){
				if (asteriscos.length < limite){
					frec = setTimeout(app.soltarAste, 1000);
					console.log(frec);
				}
			}
			
		}
		//console.log('ancho: ' + ancho + 'alto: ' + alto);
		estados = { preload: preload, create: create, update: update };
		game = new Phaser.Game(ancho, alto, Phaser.CANVAS, 'phaser', estados);

	},
	
	numeroAleatorio: function(min, max){
		return Math.round(Math.random() * (max - min) + min);
	},
	
	soltarAste: function(){
		gravedadAste = app.numeroAleatorio(min, max);
		var asterisco = asteriscos.create(app.inicioX(), 25, 'aste');
		game.physics.arcade.enable(asterisco);

		asterisco.body.gravity.y = gravedadAste;
		frec = 0;
	},
	
	incrementaPuntuacion: function(barra, aste){
		aste.destroy();
		puntuacion +=1;
		scoreText.text = "Puntuación: " + puntuacion;
	},
	
	decrementaVida: function(suelo, aste){
		aste.destroy();
		vidas -=1;
		vidaText.text = "Vidas: " + vidas;
	},
	inicioX: function(){
		return app.numeroAleatorioHasta(ancho - D_barra );
	},
  
	numeroAleatorioHasta: function(limite){
		return Math.floor(Math.random() * limite);
	}
};

if ('addEventListener' in document) {
    document.addEventListener('deviceready', function() {
        app.inicio();
    }, false);
}
